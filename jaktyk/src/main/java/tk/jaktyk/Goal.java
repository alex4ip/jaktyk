package tk.jaktyk;

import java.awt.Color;
import java.awt.Graphics;
//import java.util.Formatter;

import tk.jaktyk.collisionphysics.CollisionPhysics;
import tk.jaktyk.collisionphysics.CollisionResponse;

public class Goal  {
//	extends Ball
	   float x, y;           // Goal's center x and y (package access)
	   float speedX, speedY; // Goal's speed per step in x and y (package access)
	   float radius;         // Goal's radius or half side square (package access)
	   private Color color;  // Goal's color
	   private static final Color DEFAULT_COLOR = Color.MAGENTA;
	   char charKey='f';
	   
	      
	   // For collision detection and response
	   // Maintain the response of the earliest collision detected 
	   //  by this ball instance. Only the first collision matters! (package access)
	   CollisionResponse earliestCollisionResponse = new CollisionResponse();
	   
	   
	   /**
	    * Constructor: For user friendliness, user specifies velocity in speed and
	    * moveAngle in usual Cartesian coordinates. Need to convert to speedX and
	    * speedY in Java graphics coordinates for ease of operation.
	    */
	   public Goal(float x, float y, float radius, float speed,
			float angleInDegree, Color color) {
//		super(x, y, radius, speed, angleInDegree, color);
		   this.x = x;
		   this.y = y;
		   // Convert (speed, angle) to (x, y), with y-axis inverted
		   this.speedX = (float)(speed * Math.cos(Math.toRadians(angleInDegree)));
		   this.speedY = (float)(-speed * (float)Math.sin(Math.toRadians(angleInDegree)));
		   this.radius = radius;
		   this.color = color;
	}

	   /** Constructor with the default color */
	   public Goal(float x, float y, float radius, float speed, float angleInDegree) {
		   this(x, y, radius, speed, angleInDegree, DEFAULT_COLOR);
	   }


	   /** Constructor NoMovie with the default color */	   
	   public Goal(float x, float y, float radius, char charKey) {

		   this(x, y, radius, 0f, 0f, DEFAULT_COLOR);
		   this.charKey = charKey;
	   }

	   public Goal(float x, float y, float radius) {

		   this(x, y, radius, 0f, 0f, DEFAULT_COLOR);

	   }
	   
//	   private CollisionResponse tempResponse = new CollisionResponse(); 
//
//	   /**
//	    * Check if this ball collides with the container box in the coming time-step.
//	    * 
//	    * @param box: outer rectangular container.
//	    * @param timeLimit: upperbound of the time interval.
//	    */
//	   public void intersect(ContainerBox box, float timeLimit) {
//	      // Call movingPointIntersectsRectangleOuter, which returns the 
//	      // earliest collision to one of the 4 borders, if collision detected.
//	       CollisionPhysics.pointIntersectsRectangleOuter(x, y, speedX, speedY, radius,
//	box.minX, box.minY, box.maxX, box.maxY, timeLimit, tempResponse);
//	      if (tempResponse.t < earliestCollisionResponse.t) {
//	         earliestCollisionResponse.copy(tempResponse);
//	      }
//
//	   }
	   
	   
	   private CollisionResponse thisResponse = new CollisionResponse(); 
	   private CollisionResponse anotherResponse = new CollisionResponse(); 
	   
	   public void intersect(Ball another, float timeLimit) {
		      // Call movingPointIntersectsMovingPoint() with timeLimit.
		      // Use thisResponse and anotherResponse, as the working copies, to store the
		      // responses of this ball and another ball, respectively.
		      // Check if this collision is the earliest collision, and update the ball's
		      // earliestCollisionResponse accordingly.
		       CollisionPhysics.pointIntersectsMovingPoint(
		              this.x, this.y, this.speedX, this.speedY, this.radius,
		              another.x, another.y, another.speedX, another.speedY, another.radius,
		              timeLimit, thisResponse, anotherResponse);
//		      
//		      if (anotherResponse.t < another.earliestCollisionResponse.t) {
//		            another.earliestCollisionResponse.copy(anotherResponse);
//		      }
//		      if (thisResponse.t < this.earliestCollisionResponse.t) {
//		            this.earliestCollisionResponse.copy(thisResponse);
//		      }	   
		   
		   
	   }
	   	   

	   public void update(float time) {
		      // Check if this ball is responsible for the first collision?
		      if (earliestCollisionResponse.t <= time) { // FIXME: threshold?
		         // This ball collided, get the new position and speed
		         this.x = earliestCollisionResponse.getNewX(this.x, this.speedX);
		         this.y = earliestCollisionResponse.getNewY(this.y, this.speedY);
		         this.speedX = earliestCollisionResponse.newSpeedX;
		         this.speedY = earliestCollisionResponse.newSpeedY;
		      } else {
		         // This ball does not involve in a collision. Move straight.
		         this.x += this.speedX * time;         
		         this.y += this.speedY * time;         
		      }
		      // Clear for the next collision detection
		      earliestCollisionResponse.reset();
		   }

		   /** Draw itself using the given graphics context. */
		   public void draw(Graphics g) {
		      g.setColor(color);
		      g.fillOval((int)(x - radius), (int)(y - radius), (int)(2 * radius),
		            (int)(2 * radius));
		   }

//
//		   /** Describe itself. */
//		   public String toString() {
//		      sb.delete(0, sb.length());
//		      formatter.format("@(%3.0f,%3.0f) r=%3.0f V=(%3.0f,%3.0f) " +
//		            "S=%4.1f \u0398=%4.0f KE=%3.0f", 
//		            x, y, radius, speedX, speedY, getSpeed(), getMoveAngle(),
//		            getKineticEnergy());  // \u0398 is theta
//		      return sb.toString();
//		   }
//		   private StringBuilder sb = new StringBuilder();
//		   private Formatter formatter = new Formatter(sb);

		}
